'use strict'

const $logger = Symbol('logger')

function registerXrefAuditorTreeProcessor (file) {
  return function () {
    this.process((doc) => {
      const converter = doc.getConverter()
      const sourceLocation = doc.getSourceLocation()
      const resourceRefCallbackKey = Reflect.ownKeys(converter).find((it) =>
        typeof it === 'symbol' && it.description === 'resourceRefCallback'
      )
      const imageRefCallbackKey = Reflect.ownKeys(converter).find((it) =>
        typeof it === 'symbol' && it.description === 'imageRefCallback'
      )
      const resourceRefCallback = converter[resourceRefCallbackKey]
      const imageRefCallback = converter[imageRefCallbackKey]
      converter[resourceRefCallbackKey] = (refspec, text) => {
        if (refspec.includes('@')) logVersionedXref(converter, refspec, 'xref', sourceLocation)
        return resourceRefCallback(refspec, text)
      }
      converter[imageRefCallbackKey] = (refspec) => {
        if (refspec.includes('@')) logVersionedXref(converter, refspec, 'image', sourceLocation)
        return imageRefCallback(refspec)
      }
    })
  }
}

function getLogger (converter) {
  return converter[$logger] || (converter[$logger] = converter.$logger())
}

function logVersionedXref (converter, refspec, type, sourceLocation) {
  let msg = 'target of ' + type + ' includes version: ' + refspec
  if (sourceLocation) msg = converter.$message_with_context(msg, Opal.hash({ source_location: sourceLocation }))
  getLogger(converter).warn(msg)
}

function toProc (fn) {
  return Object.defineProperty(fn, '$$arity', { value: fn.length })
}

module.exports.register = function (registry, { file }) {
  if (!('groups' in registry)) return // not Antora
  registry.groups.$send('[]=', 'xref_auditor', toProc(() => {
    registry.treeProcessor(registerXrefAuditorTreeProcessor(file))
  }))
}
