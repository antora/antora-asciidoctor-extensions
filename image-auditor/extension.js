'use strict'

const $logger = Symbol('logger')

function registerEnableSourcemapPreprocessor () {
  return function () {
    this.process((doc) => {
      doc.setSourcemap(true)
    })
  }
}

function registerImageAuditorTreeProcessor (contentCatalog, file) {
  return function () {
    this.process((doc) => {
      const logger = doc.getLogger()
      doc.findBy({ context: 'image', traverse_documents: true }).forEach((image) => {
        const imageRef = image.getAttribute('target')
        const imageFile = contentCatalog.resolveResource(imageRef, file.src, 'image', ['image'])
        if (!imageFile) return
        const altText = image.getAttribute('alt')
        const defaultAltText = image.getAttribute('default-alt')
        const msg = 'image: ' + imageRef + '; alt: ' + (altText === defaultAltText ? '(not set)' : altText)
        logger.log('info', doc.createLogMessage(msg, { source_location: image.getSourceLocation() }))
      })
    })
  }
}

function toProc (fn) {
  return Object.defineProperty(fn, '$$arity', { value: fn.length })
}

module.exports.register = function (registry, { contentCatalog, file }) {
  if (!('groups' in registry)) return // not Antora
  registry.groups.$send('[]=', 'image_auditor', toProc(() => {
    registry.preprocessor(registerEnableSourcemapPreprocessor())
    registry.treeProcessor(registerImageAuditorTreeProcessor(contentCatalog, file))
  }))
}
